﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace VirginPulse
{
    class TestDriver
    {
        IWebDriver driver;
        [SetUp]
        public void Initialize()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("disable-infobars");
            options.AddUserProfilePreference("credentials_enable_service", false);
            options.AddUserProfilePreference("profile.password_manager_enabled", false);
            driver = new ChromeDriver(options);
        }

        /*[Test]
        public void CredentialTest()
        {
            driver.Url = ConfigurationManager.AppSettings["URL"];

            Login();
            IWebElement logo = driver.FindElement(By.CssSelector("#core-menuitem-logo"));
            Assert.AreEqual("img",logo.TagName);

        }*/

        [Test]
        public void DailyEntry()
        {
            int initialPointTotal = 0;
            int finalPointTotal = 0;

            #region Login

            driver.Url = ConfigurationManager.AppSettings["URL"];
            driver.Manage().Window.Maximize();

            Login();

            #endregion

            #region DailyCards

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            try
            {
                // Trophy popup window
                IWebElement popup = driver.FindElement(By.CssSelector("#trophypopup > ng-edge-bubble-celebration > div > div > div.close-trophy-popup-wrapper"));
                if (popup.Displayed)
                {
                    popup.Click();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Trophy window was not found");
            }

            try
            {
                // get initial points
                initialPointTotal = getPoints();

                // Daily cards
                checkTrophyPopup();
                IWebElement dailyCards = driver.FindElement(By.CssSelector("#triggerCloseCurtain"));
                dailyCards.Click();
                Thread.Sleep(3000);
                checkTrophyPopup();
                var dailyCards2 = driver.FindElements(By.CssSelector("#triggerCloseCurtain"));
                dailyCards2[1].Click();
                checkTrophyPopup();

                // Continue past daily cards to healthy habits
                Thread.Sleep(3000);
                IWebElement trackHealthyHabits = driver.FindElement(By.CssSelector("#daily-tips-slider > thats-wrap-card > div > div.rewards-btn-container > button"));
                Assert.AreEqual("TRACK HEALTHY HABITS", trackHealthyHabits.Text);
                trackHealthyHabits.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine("Didn't find the track healthy habits button on the daily cards, skipping directly to healthy habits ");
                IWebElement skipToHealthyHabits = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div.home-cards-wrapper > tour > div.dialy-tips-wrapper.hh-wrapper.ng-scope > div.dialy-tips-circle.hh-icon-wrapper > div.daily-tips-title.ng-scope"));
                skipToHealthyHabits.Click();
            }

            #endregion

            #region HealthyHabits

            try
            {
                //start the day right
                IWebElement habitBox = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div:nth-child(4) > home-healthy-habits > div > div > div:nth-child(1) > home-healthy-habit-tile > div"));
                Actions action = new Actions(driver);
                action.MoveToElement(habitBox).Perform();
                Thread.Sleep(1000);

                IWebElement healthyHabitYes = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div:nth-child(4) > home-healthy-habits > div > div > div:nth-child(1) > home-healthy-habit-tile > div > div.home-healthy-habit-yesno.ng-scope > button.yesNo-btn.yes-btn.ng-scope"));
                healthyHabitYes.Click();
                Console.WriteLine("Clicked Yes for Healthy Habit #1");
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                //don't miss a beat
                IWebElement habitBox = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div:nth-child(4) > home-healthy-habits > div > div > div:nth-child(2) > home-healthy-habit-tile > div"));
                Actions action = new Actions(driver);
                action.MoveToElement(habitBox).Perform();
                Thread.Sleep(1000);

                IWebElement healthyHabitYes = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div:nth-child(4) > home-healthy-habits > div > div > div:nth-child(2) > home-healthy-habit-tile > div > div.home-healthy-habit-yesno.ng-scope > button.yesNo-btn.yes-btn.ng-scope"));
                healthyHabitYes.Click();
                Console.WriteLine("Clicked Yes for Healthy Habit #2");
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            try
            {
                //did you take the stairs
                IWebElement habitBox = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div:nth-child(4) > home-healthy-habits > div > div > div:nth-child(3) > home-healthy-habit-tile > div"));
                Actions action = new Actions(driver);
                action.MoveToElement(habitBox).Perform();
                Thread.Sleep(1000);

                IWebElement healthyHabitYes = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > basic-home > div > div > div:nth-child(4) > home-healthy-habits > div > div > div:nth-child(3) > home-healthy-habit-tile > div > div.home-healthy-habit-yesno.ng-scope > button.yesNo-btn.yes-btn.ng-scope"));
                healthyHabitYes.Click();
                Console.WriteLine("Clicked Yes for Healthy Habit #3");
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Thread.Sleep(5000);


            #endregion

            #region Workout

            IWebElement dailyTracking = driver.FindElement(By.CssSelector("#core-menuitem-tracking"));
            dailyTracking.Click();

            Thread.Sleep(5000);
            IWebElement bottomOfPage = driver.FindElement(By.CssSelector("body > div.app-holder.ng-scope > div.footer.ng-scope > div > div:nth-child(1) > div > img"));
            Actions actionsBottom = new Actions(driver);
            actionsBottom.MoveToElement(bottomOfPage);
            actionsBottom.Perform();

            IWebElement addWorkoutDropdown = driver.FindElement(By.CssSelector("#tracker_42 > div > div > div.title.col-md-4 > div.portlet-body.col-md-12 > steps-converter-input > div > div.activity-search-box > div.activity-input"));
            addWorkoutDropdown.Click();
            var addworkoutSearch = driver.FindElements(By.CssSelector("#tracker_42 > div > div > div.title.col-md-4 > div.portlet-body.col-md-12 > steps-converter-input > div > div.activity-search-box.active > div.activity-search-results div"));
            Random rvg = new Random();
            addworkoutSearch[rvg.Next(0, 4)].Click();

            IWebElement addWorkoutTime = driver.FindElement(By.CssSelector("#tracker_42 > div > div > div.title.col-md-4 > div.portlet-body.col-md-12 > steps-converter-input > div > div.activity-input-body > div.duration-distance-wrapper > div.duration-box > input"));
            addWorkoutTime.Clear();
            addWorkoutTime.SendKeys(rvg.Next(45,60).ToString());
            IWebElement workoutTrackTimeBtn = driver.FindElement(By.CssSelector("#tracker_42 > div > div > div.title.col-md-4 > div.portlet-body.col-md-12 > steps-converter-input > div > div.activity-input-body > button"));
            workoutTrackTimeBtn.Click();
            Console.WriteLine("Added workout");
            Thread.Sleep(5000);

            #endregion


            #region currentPointsLevel
            
            finalPointTotal = getPoints();

            //TODO: test this new point checking logic
            int daysToNextLevel = 0;
            int pointsPerDay = 70;
            int pointDiff = finalPointTotal - initialPointTotal;
            Console.WriteLine("Points Earned today: " + pointDiff);
            Assert.AreEqual(pointsPerDay, pointDiff);

            if (finalPointTotal != 0)
            {
                if (finalPointTotal < 5000)
                {
                    daysToNextLevel = (5000 - finalPointTotal) / pointsPerDay;
                    Console.WriteLine("You are level 0, and {0} points away from level 1. Approximately {1} days away.", 5000 - finalPointTotal, daysToNextLevel);
                }
                else if (finalPointTotal < 15000)
                {
                    daysToNextLevel = (15000 - finalPointTotal) / pointsPerDay;
                    Console.WriteLine("You are level 1, and {0} points away from level 2 ($100). Approximately {1} days away.", 15000 - finalPointTotal, daysToNextLevel);
                }
                else if (finalPointTotal < 30000)
                {
                    daysToNextLevel = (30000 - finalPointTotal) / pointsPerDay;
                    Console.WriteLine("You are level 2, and {0} points away from level 3 ($200). Approximately {1} days away.", 30000 - finalPointTotal, daysToNextLevel);
                }
                else if (finalPointTotal < 45000)
                {
                    daysToNextLevel = (45000 - finalPointTotal) / pointsPerDay;
                    Console.WriteLine("You are level 3, and {0} points away from level 4 ($300). Approximately {1} days away.", 45000 - finalPointTotal, daysToNextLevel);
                }
            }

            #endregion

        }
        [TearDown]
        public void EndTest()
        {
            driver.Close();
            driver.Quit();
        }






        private void Login()
        {
            var userpass = CredentialUtil.GetCredential(ConfigurationManager.AppSettings["VirginPulseCredential"]);
            IWebElement username = driver.FindElement(By.CssSelector("#oUserID"));
            username.SendKeys(userpass.Username);
            IWebElement password = driver.FindElement(By.CssSelector("#txtPlainPassword"));
            password.Click();
            IWebElement hiddenField = driver.FindElement(By.CssSelector("#oPwdID"));
            hiddenField.SendKeys(userpass.Password);
            hiddenField.Click();
            Thread.Sleep(1000);
            hiddenField.SendKeys(Keys.Return);
            Thread.Sleep(5000);
        }

        private Int32 getPoints()
        {
            IWebElement topOfPage = driver.FindElement(By.CssSelector("#basic-menu-gamebar > div.basic-menu-rewards-total-button > rewards-button > div > div > div.rewards-total-value"));
            Actions actionsTop = new Actions(driver);
            actionsTop.MoveToElement(topOfPage);
            actionsTop.Perform();
            topOfPage.Click();

            IWebElement points = driver.FindElement(By.CssSelector("#page-wrapper > div > div > div > div > div.col-xs-10 > div.panel.panel-tabbed-content > div.panel-body.ng-scope > div > div.rewards-earn-progress-bar > rewards-progress-bar > div:nth-child(2) > div.rewards-progress-bar > div.rewards-progress-bar-points-popover > div.rewards-progress-bar-points-popover-content > div.ng-scope.ng-binding"));
            string[] total = points.Text.Split(null);
            Console.WriteLine("Current points total is: " + total[0]);
            Int32.TryParse(total[0], out int pointTotal);

            IWebElement returnToHomePage = driver.FindElement(By.CssSelector("#core-menuitem-logo"));
            returnToHomePage.Click();
            return pointTotal;
        }

        public void checkTrophyPopup()
        {
            try
            {
                IWebElement trophyPopup = driver.FindElement(By.CssSelector("#trophypopup > ng-edge-bubble-celebration > div > div > div.close-trophy-popup-wrapper"));
                if (trophyPopup != null)
                {
                    trophyPopup.Click();
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}
