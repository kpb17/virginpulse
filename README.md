# README #


# What is this repository for? #

### Quick summary ###

This program clicks through the daily cards, healthy habits, and enters a daily workout

### Version ###

0.1 (Alpha)

# How do I get set up? #

### Configuration ###

Add a general credential to the Windows Credential Manager with your virgin pulse login.![credentials.PNG](https://bitbucket.org/repo/Loo46zb/images/154690793-credentials.PNG)

### Dependencies ###

Set up 3 healthy habits, as the first 3 things to track, and then the workout card.
To do this, hit the tracking button in the top menu, and select/re-order your options so you have 3 Yes/No habits followed by the workout section. Like so:![tracking.PNG](https://bitbucket.org/repo/Loo46zb/images/4219525440-tracking.PNG)

### How to run tests ###

Clone repo, build solution. The required NuGet packages should be downloads from nuget.org. Then you can run either the CredentialTest (just tries logging into the page) or the DailyEntry test which enters in the daily activities.

### Deployment instructions ###
* Download the 
[NUnit Console
](https://github.com/nunit/nunit-console/releases/tag/3.6.1)
* Copy the batch script from the repository and edit your file paths. Update 1) the location of your NUnit console install, 2) the location of your compiled test dll, 3) your log directory locations.
* Add the VirginPulseCredential to your Windows Credential Manager which stores your username and password for VirginPulse.
* At this point you should be able to run the test manually and watch it work.
* Next up, you can create a scheduled task using the Windows Task Scheduler. Here is my job, but feel free to customize and improve it.![virginpulse.png](https://bitbucket.org/repo/Loo46zb/images/3385197246-virginpulse.png)



# Who do I talk to? #

### Repo owner or admin ###

Kristian